﻿#include <iostream>

using namespace std;

class Animal
{
public:
	virtual void Voice()
	{
		cout << " " << endl;
	}
};

class Dog : public Animal
{
	void Voice() override
	{
		cout << "Dog does Woof!\n";
	}
};

class Chicken : public Animal
{
	void Voice() override
	{
		cout << "Chicken does co-co\n";
	}
};

class Snake : public Animal
{
	void Voice() override
	{
		cout << "Snake does shhhhh\n";
	}
};

void main()
{
	Animal* VoiceDog = new Dog();
	Animal* VoiceChicken = new Chicken();
	Animal* VoiceSnake = new Snake();

	Animal* Voice[] = { VoiceDog , VoiceChicken, VoiceSnake };
	int i;
	for (i = 0; i < 3; i++)
	{
		Voice[i]->Voice();
	}
}